FROM chatbots/base

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . /usr/src/app

# Build front
RUN npm run build

# Run APP
EXPOSE 4200
CMD ["pm2-docker", "process.json", "--only", "production"]
