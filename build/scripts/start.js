const signale = require('signale')

const server = require('../../server/app')

signale.await('Starting server...')
server.listen(process.env.PORT, () => {
    signale.success(`Server is running on PORT: ${process.env.PORT}`)
})
