const { Router } = require('express')
const signale = require('signale')
const {
    getItem, putItem, queryItems, getS3Image
} = require('../events/utils')

const app = require('../app')
const router = Router()

function getStringDate() {
    const d = new Date()
    let month = '' + (d.getMonth() + 1)
    let day = '' + d.getDate()
    const year = d.getFullYear()
    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day
    return [year, month, day].join('/')
}

router.get('/dashboard/item/:sessionId/:startTime', async (req, res) => {
    const params = {
        TableName: process.env.ANALYSIS_TABLE,
        Key: {
            sessionId: req.params.sessionId,
            startTime: req.params.startTime.replace(/_/g, '/')
        }
    }

    try {
        const dbRes = await getItem(params)
        console.log(dbRes)
        console.log(req.params.sessionId)
        res.send(dbRes)
    } catch (err) {
        signale.debug(err)
    }
})

// handles get request on items
router.get('/dashboard/items', async (req, res) => {
    const params = {
        TableName: process.env.ANALYSIS_TABLE,
        IndexName: 'humanReadableStartTime-index',
        KeyConditionExpression:'#humanReadableStartTime = :humanReadableStartTimeValue',
        ExpressionAttributeNames: { '#humanReadableStartTime':'humanReadableStartTime' },
        ExpressionAttributeValues: { ':humanReadableStartTimeValue':getStringDate() }
    }

    try {
        const dbRes = await queryItems(params)
        res.send(dbRes)
    } catch (err) {
        signale.debug(err)
    }
})

router.get('/dashboard/images', async (req, res) => {
    try {
        const s3Res = await getS3Image({ Key: req.headers.s3imagekey, Expires: 3600 })
        res.send(s3Res)
    } catch (error) {
        signale.debug(error)
    }
})

// handles post request on verify
router.post('/dashboard/verify', async (req, res) => {
    const item = await getItem({
        TableName: process.env.ANALYSIS_TABLE,
        Key: {
            sessionId: req.body.sessionId,
            startTime: req.body.startTime
        }
    })

    const { Item } = item
    Item.status = {
        accepted: true,
        newResult: req.body.newResult
    }

    const params = {
        TableName: process.env.ANALYSIS_TABLE,
        Item
    }

    await putItem(params)
    signale.debug(req.body.sessionId)
    res.send({})
})

// handles post request on reject
router.post('/dashboard/reject', async (req, res) => {
    const item = await getItem({
        TableName: process.env.ANALYSIS_TABLE,
        Key: {
            sessionId: req.body.sessionId,
            startTime: req.body.startTime
        }
    })

    const { Item } = item
    Item.status = {
        accepted: false,
        reason: req.body.reason
    }

    const params = {
        TableName: process.env.ANALYSIS_TABLE,
        Item
    }

    await putItem(params)
    signale.debug(req.body.sessionId)
    res.send({})
})

module.exports = router
