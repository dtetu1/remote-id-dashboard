const events = require('./events')
const dashboard = require('./dashboard')


module.exports = (app) => {
    app.use('/api', events)
    app.use('/api', dashboard)

    app.all('/api/*', (req, res) => {
        res.status(404).send('API Not Found')
    })
}
