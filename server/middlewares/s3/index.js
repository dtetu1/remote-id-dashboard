const moment = require('moment')
const uuidv1 = require('uuid/v1')
const AWS = require('aws-sdk')

const s3Bucket = new AWS.S3({
    region: 'eu-central-1',
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    params: {
        Bucket: process.env.S3_BUCKET
    }
})


const upload = data => (
    new Promise((resolve, reject) => {
        s3Bucket.putObject(data, (err, s3Data) => {
            if (err) {
                reject(err)
            } else {
                resolve(s3Data)
            }
        })
    })
)

const uploadImage = async (imageStr, filename) => {
    const buf = Buffer.from(imageStr.replace(/^data:image\/\w+;base64,/, ''), 'base64')

    const data = {
        Key: `${filename}`,
        Body: buf,
        ContentEncoding: 'base64',
        // ContentType: 'image/png'
    }

    await upload(data)
}

const uploadText = async (textStr, filename) => {
    const buf = Buffer.from(textStr, 'utf8')

    const data = {
        Key: `${filename}.txt`,
        Body: buf,
        ContentEncoding: 'utf8',
        ContentType: 'text/plain'
    }

    await upload(data)
}

const saveInS3 = async (req, res, step) => {
    const {
        success,
        data,
        error,
        saveAttachment
    } = res.locals

    if (!step) {
        const { frontImage } = req.body
        const currentDate = moment.utc().format('YYYY/MM/DD/HH:mm:ss')

        uploadImage(frontImage, `pulsar-without-face/${currentDate}/image`)
        res.json({ ok: 1 })
    }

    if (step === 'front-fail') {
        const { frontImage } = req.body
        const currentDate = moment.utc().format('YYYY/MM/DD/HH:mm:ss')

        uploadImage(frontImage, `pulsar-front/${currentDate}/${uuidv1()}/image`)
        res.json({ ok: 1 })
    }

    if (step === 'passport-id' && !saveAttachment) {
        if (success) {
            res.json({ ...data })
        } else {
            const { statusCode, message } = error
            res.status(statusCode).json({ message })
        }

        return
    }

    if (step === 'card-id' || step === 'passport-id') {
        const currentDate = moment.utc().format('YYYY/MM/DD/HH:mm:ss')
        const s3path = `pulsar/${currentDate}/${uuidv1()}`

        res.setHeader('s3path', s3path)

        if (step === 'card-id') {
            const { frontImage, mrzImage } = req.body

            await uploadImage(frontImage, `${s3path}/id-card/front`)
            await uploadImage(mrzImage, `${s3path}/id-card/back`)

            if (success) {
                await uploadText(JSON.stringify(data), `${s3path}/id-card/result`)
                res.json({ ...data })
            } else {
                const { statusCode, message } = error
                res.status(statusCode).json({ message })
            }
        } else {
            const { passport } = req.body

            await uploadImage(passport, `${s3path}/passport/front`)

            if (success) {
                await uploadText(JSON.stringify(data), `${s3path}/passport/result`)
                res.json({ ...data })
            } else {
                const { statusCode, message } = error
                res.status(statusCode).json({ message })
            }
        }
    } else if (step === 'face-id') {
        const { selfie } = req.body
        const { s3path } = req.headers

        if (!s3path) {
            console.log('\n\nNO s3 path\n\n')
        }

        await uploadImage(selfie, `${s3path}/face/selfie`)

        if (success) {
            await uploadText(JSON.stringify(data), `${s3path}/face/result`)
            res.json({ ...data })
        } else {
            const { statusCode, message } = error
            res.status(statusCode).json({ message })
        }
    }
}

module.exports = {
    saveInS3
}
