/* eslint-disable max-len */
// eslint-disable-next-line no-unused-vars
const uuidv1 = require('uuid/v1')
const AWS = require('aws-sdk')
const signale = require('signale')


const s3Bucket = new AWS.S3({
    region: 'eu-central-1',
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    params: {
        Bucket: process.env.S3_BUCKET
    }
})

const upload = data => (
    new Promise((resolve, reject) => {
        s3Bucket.putObject(data, (err, s3Data) => {
            if (err) {
                reject(err)
            } else {
                resolve(s3Data)
            }
        })
    })
)

const uploadImage = async (imageStr, filename) => {
    const buf = Buffer.from(imageStr.replace(/^data:image\/\w+;base64,/, ''), 'base64')

    const data = {
        Key: `${filename}`,
        Body: buf,
        ContentEncoding: 'base64',
        // ContentType: 'image/png'
    }

    await upload(data)
}

const uploadText = async (textStr, filename) => {
    const buf = Buffer.from(textStr, 'utf8')

    const data = {
        Key: `${filename}.txt`,
        Body: buf,
        ContentEncoding: 'utf8',
        ContentType: 'text/plain'
    }

    await upload(data)
}

// eslint-disable-next-line max-len
const saveInS3 = async (req, res, sessionId, startTime, cardType, frontImage, frontSideAttemps, mrzImage, mrzInfo, backSideAttempts, selfieImage, passportImage) => {
    // signale.debug('>>>>>>>>>>>>sessionId', sessionId)
    // signale.debug('>>>>>>>>>>>>startTime', startTime)
    // signale.debug('>>>>>>>>>>>>cardType', cardType)
    // signale.debug('>>>>>>>>>>>>frontImage', frontImage !== undefined)
    // signale.debug('>>>>>>>>>>>>frontSideAttemps', frontSideAttemps)
    // signale.debug('>>>>>>>>>>>>mrzImage', mrzImage !== undefined)
    // signale.debug('>>>>>>>>>>>>backSideAttempts', backSideAttempts)
    // signale.debug('>>>>>>>>>>>>selfieImage', selfieImage !== undefined)
    // signale.debug('>>>>>>>>>>>>passportImage', passportImage !== undefined)


    if (cardType === 'card-id') {
        signale.debug('frontImage is not undefined', frontImage !== undefined)

        const links = {}
        if (frontImage) {
            signale.debug('came here ready to upload front', typeof frontImage)
            // eslint-disable-next-line max-len
            uploadImage(frontImage, `pulsar-test/${startTime}/${sessionId}/id-card/front/frontImage${frontSideAttemps}.png`)
            // uploadText(JSON.stringify(mrzInfo), `pulsar-test/${startTime}/${sessionId}/id-card/front/result`)
            // eslint-disable-next-line max-len
            links.frontImageLink = `pulsar-test/${startTime}/${sessionId}/id-card/front/frontImage${frontSideAttemps}.png`
        }
        if (mrzImage) {
            uploadImage(mrzImage, `pulsar-test/${startTime}/${sessionId}/id-card/back/backImage${backSideAttempts}.png`)
            uploadText(JSON.stringify(mrzInfo), `pulsar-test/${startTime}/${sessionId}/id-card/back/result`)
            links.mrzImageLink = `pulsar-test/${startTime}/${sessionId}/id-card/back/backImage${backSideAttempts}.png`
        }
        if (selfieImage) {
            uploadImage(selfieImage, `pulsar-test/${startTime}/${sessionId}/face/selfie.png`)
            // uploadText(JSON.stringify(mrzInfo), `pulsar-test/${startTime}/${sessionId}/face/result`)
            links.selfieImageLink = `pulsar-test/${startTime}/${sessionId}/face/selfie.png`
        }
        return links
    }

    if (cardType === 'passport') {
        console.log('passport imaaaaage', passportImage !== undefined)

        const links = {}
        if (passportImage) {
            // eslint-disable-next-line max-len
            console.log('hello', passportImage.substr(0, 100))
            // eslint-disable-next-line max-len
            uploadImage(passportImage, `pulsar-test/${startTime}/${sessionId}/passport/frontImage${frontSideAttemps}.png`)
            uploadText(JSON.stringify(mrzInfo), `pulsar-test/${startTime}/${sessionId}/passport/result`)
            links.frontImageLink = `pulsar-test/${startTime}/${sessionId}/passport/frontImage${frontSideAttemps}.png`
        }

        if (selfieImage) {
            uploadImage(selfieImage, `pulsar-test/${startTime}/${sessionId}/face/selfie.png`)
            // uploadText(JSON.stringify(mrzInfo), `pulsar-test/${startTime}/${sessionId}/face/result`)
            links.selfieImageLink = `pulsar-test/${startTime}/${sessionId}/face/selfie.png`
        }

        return links
    }

    return []
}


module.exports = {
    saveInS3
}
