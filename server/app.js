const express = require('express')
const path = require('path')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const compress = require('compression')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const dotenv = require('dotenv')
const signale = require('signale')
const basicAuth = require('express-basic-auth')

const webpackConfig = require('../build/webpack.config')
const project = require('../project.config')

const app = express()

// Load Environment Variables
dotenv.config({
    path: `${project.basePath}/config/.${process.env.NODE_ENV}`
})

// App Setup
app.use(compress())
app.use(bodyParser.json({
    limit: '50mb',
    extended: true
}))
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(cookieParser())

const server = require('http').createServer(app)
const io = require('socket.io')(server)
// io.set( 'origins', '*localhost:4200  ' );

// server.listen(4001)


io.on('connection', socket => {
    socket.emit('client-connect', {sessionId: '0796b640-393c-11ea-b5b3-afe30769e832', startTime: '2020/01/17/15:14:17'})

    socket.on('dashboard-verify', (newResult) => {
        io.sockets.emit('client-verify', newResult)
    })
})




// Apply API Routes
require('./routes')(app)

// ------------------------------------
// Apply Webpack HMR Middleware
// ------------------------------------
if (project.env === 'development') {
    const compiler = webpack(webpackConfig)

    signale.await('Enabling webpack development and HMR middleware')
    app.use(webpackDevMiddleware(compiler, {
        publicPath: webpackConfig.output.publicPath,
        contentBase: path.resolve(project.basePath, project.srcDir),
        hot: true,
        quiet: false,
        noInfo: false,
        lazy: false,
        stats: 'normal',
    }))
    app.use(webpackHotMiddleware(compiler, {
        path: '/__webpack_hmr'
    }))

    // Serve static assets from ~/public since Webpack is unaware of
    // these files. This middleware doesn't need to be enabled outside
    // of development since this directory will be copied into ~/dist
    // when the application is compiled.
    app.use([express.static(path.resolve(project.basePath, 'public'))])

    // This rewrites all routes requests to the root /index.html file
    // (ignoring file requests). If you want to implement universal
    // rendering, you'll want to remove this middleware.
    app.use('*', (req, res, next) => {
        const filename = path.join(compiler.outputPath, 'index.html')
        compiler.outputFileSystem.readFile(filename, (err, result) => {
            if (err) {
                return next(err)
            }
            res.set('content-type', 'text/html')
            res.send(result)
            return res.end()
        })
    })
} else if (project.env === 'testing') {
    const handleUnauthorized = req => (
        req.auth ? `Credentials ${req.auth.user}:${req.auth.password} rejected` : 'No credentials provided'
    )

    const auth = basicAuth({
        challenge: true,
        realm: 'Imb4T3st4pp',
        users: {
            pulsar: process.env.PULSAR_FRONT_PASSWORD
        },
        unauthorizedResponse: handleUnauthorized
    })

    app.get('/health', (req, res) => {
        res.send('Remote ID healthy!')
    })

    app.use([auth, express.static(path.resolve(project.basePath, project.outDir))])

    app.get('*', auth, (req, res) => {
        res.sendFile(path.resolve(project.basePath, project.outDir, 'index.html'))
    })
} else {
    const handleUnauthorized = req => (
        req.auth ? `Credentials ${req.auth.user}:${req.auth.password} rejected` : 'No credentials provided'
    )

    const auth = basicAuth({
        challenge: true,
        realm: 'Imb4T3st4pp',
        users: {
            pulsar: process.env.FRONT_PASSWORD
        },
        unauthorizedResponse: handleUnauthorized
    })

    app.get('/health', (req, res) => {
        res.send('Remote ID healthy!')
    })

    app.use([auth, express.static(path.resolve(project.basePath, project.outDir))])

    app.get('*', auth, (req, res) => {
        res.sendFile(path.resolve(project.basePath, project.outDir, 'index.html'))
    })
}


module.exports = server
