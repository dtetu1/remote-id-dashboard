const aws = require('aws-sdk')

aws.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_DEFAULT_REGION
})

const docClient = new aws.DynamoDB.DocumentClient()
const s3 = new aws.S3({
    region: process.env.AWS_DEFAULT_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    params: {
        Bucket: process.env.S3_BUCKET
    }
})

const putItem = params => (
    new Promise((resolve, reject) => {
        docClient.put(params, (err, res) => {
            if (err) {
                reject(err)
            }

            resolve(res)
        })
    })
)

const getItem = params => (
    new Promise((resolve, reject) => {
        docClient.get(params, (err, res) => {
            if (err) {
                reject(err)
            }

            resolve(res)
        })
    })
)

const queryItems = params => (
    new Promise((resolve, reject) => {
        docClient.query(params, (err, res) => {
            if (err) {
                reject(err)
            }

            resolve(res)
        })
    })
)

const getS3Image = params => (
    new Promise((resolve, reject) => {
        s3.getSignedUrl('getObject', params, (err, res) => {
            if (err) {
                reject(err)
            }
            resolve(res)
        })
    })
)

module.exports = {
    putItem,
    getItem,
    queryItems,
    getS3Image
}
