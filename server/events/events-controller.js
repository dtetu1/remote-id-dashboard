/* eslint-disable max-len */

const signale = require('signale')

const { putItem, getItem } = require('./utils')
const { saveInS3 } = require('../middlewares/s3/saveInS3')

const eventController = async (req, res) => {
    const {
        sessionId,
        startTime,

        cardType,
        device,
        userAgent,

        frontScanSuccess,
        frontImage,
        frontImageUploaded,
        frontSideAttemps,

        backScanSuccess,
        mrzImage,
        mrzInfo,
        backImageUploaded,
        backSideAttempts,

        passportImage,

        selfieImage,
        matchSuccess,
    } = req.body

    const Key = {
        sessionId,
        startTime
    }

    const dbRes = await getItem({
        TableName: process.env.ANALYSIS_TABLE,
        Key,
    })

    // signale.debug("Item returned value", Item)

    signale.debug('-------------mrzInfo------------------')
    signale.debug('mrzInfo', mrzInfo)

    if (passportImage) {
        signale.debug('passportImage:', passportImage.substring(0, 100))
    } else {
        signale.debug('passportImage:', passportImage)
    }

    let Item = null
    if (dbRes.Item) {
        Item = { ...dbRes.Item }
        signale.debug('-------------Item------------------', Item)

        Item[`frontImageUploaded${Number(frontSideAttemps)}`] = frontImageUploaded
        Item[`frontScanSuccess${Number(frontSideAttemps)}`] = frontScanSuccess

        Item[`backImageUploaded${Number(backSideAttempts)}`] = backImageUploaded
        Item[`backScanSuccess${Number(backSideAttempts)}`] = backScanSuccess
        Item.matchSuccess = matchSuccess
    }


    if (Item) {
        // signale.debug('heeeey:', passportImage !== undefined)

        const links = await saveInS3(req, res, sessionId, startTime, Item.cardType, frontImage, frontSideAttemps, mrzImage, mrzInfo, backSideAttempts, selfieImage)
        const prefix = 'https://remote-id.s3-eu-west-1.amazonaws.com/' // This should be changed
        if (links.frontImageLink) {
            Item[`frontImageLink${Number(frontSideAttemps)}`] = prefix + links.frontImageLink
        } else if (links.mrzImageLink) {
            Item[`backImageLink${Number(backSideAttempts)}`] = prefix + links.mrzImageLink
        } else if (links.selfieImageLink) {
            Item.selfieImageLink = prefix + links.selfieImageLink
        }

        if (mrzInfo) {
            Item.result = JSON.stringify(mrzInfo)
        }

        await putItem({
            TableName: process.env.ANALYSIS_TABLE,
            Item
        })
    } else {
        signale.debug('-------------base is empty------------------')
        Item = Key
        Item.cardType = cardType
        Item.device = device
        Item.humanReadableStartTime = startTime.substring(0, 10)
        Item.userAgent = userAgent
        Item.client = 'pulsar-demo-new'
        await putItem({
            TableName: process.env.ANALYSIS_TABLE,
            Item
        })
    }

    /*
    signale.debug('-------------Start------------------')
    signale.debug('Verification startTime:', startTime)
    signale.debug('sessionId:', sessionId)
    signale.debug('cardType:', cardType)
    signale.debug('device:', device)
    signale.debug('userAgent', userAgent)

    signale.debug('-------------Card Front------------------')
    signale.debug('frontScanSuccess', frontScanSuccess)
    signale.debug('frontImage', frontImage !== undefined)
    signale.debug('frontImageUploaded', frontImageUploaded)
    signale.debug('frontSideAttemps', frontSideAttemps)

    signale.debug('-------------Card Back------------------')
    signale.debug('backScanSuccess', backScanSuccess)
    signale.debug('backImage', mrzImage !== undefined)
    signale.debug('mrzinfo', mrzInfo)
    signale.debug('backImageUploaded', backImageUploaded)
    signale.debug('backSideAttempts', backSideAttempts)

    signale.debug('-------------Face Rec------------------')
    signale.debug('selfieImage', selfieImage !== undefined)
    signale.debug('matchSuccess', matchSuccess)

    signale.debug('---------------End------------------')
    */
    res.json({ message: 'ok', startTime })
}

module.exports = {
    eventController
}
