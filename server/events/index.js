const { Router } = require('express')
const { eventController } = require('./events-controller')

const router = Router()

router.post('/events', eventController)

module.exports = router
