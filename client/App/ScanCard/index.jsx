import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import { withRouter } from 'react-router'
import { init, switchSide, stopTracking } from './JsFeat'

import Stepper from '../Common/Stepper'

import {
    readMRZ
} from './actions'

import './index.scss'

class ScanCard extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            scanDetails: {
                activeSide: 'front',
                info: 'დაასკანერე ID ბარათის წინა მხარე'
            }
        }

    }

    componentDidMount() {
        init('front', (res) => {
            this.onImageReady(res)
        })
    }

    componentWillUnmount() {
        stopTracking()
    }

    onImageReady(base64Image) {
        if (base64Image) {
            if (this.state.scanDetails.activeSide === 'front') {
                this.setState({
                    frontImage: base64Image,
                    waitUserAction: true,
                    popupMessage: 'წინა მხარე წარმატებით დასკანერდა'
                })
            } else {
                const imageStr = base64Image.substr(22, base64Image.length)
                this.props.readMRZ(imageStr)
            }
        }
    }

    processImage() {
        init('mrz', (res) => { this.onImageReady(res) })

        this.setState({
            scanDetails: {
                activeSide: 'mrz',
                info: 'დაასკანერე ID ბარათის უკანა მხარე'
            },
            waitUserAction: false
        })
    }

    reInitState() {
        init('mrz', (res) => { this.onImageReady(res) })

        this.setState({
            // scanDetails: {
            //     activeSide: 'front',
            //     info: 'დაასკანერე ID ბარათის წინა მხარე'
            // },
            waitUserAction: false,
            mustReInit: false
        })
    }

    goToScanFace() {
        const { frontImage } = this.state

        console.log('front image is: ', frontImage)

        this.props.history.push({
            pathname: '/scan-face',
            state: { frontImage }
        })
    }

    nextAction() {
        const { scanDetails, mustReInit } = this.state

        if (scanDetails.activeSide === 'front') {
            this.processImage()
        } else if (mustReInit) {
            this.reInitState()
        } else {
            this.goToScanFace()
        }
    }

    UNSAFE_componentWillReceiveProps(props) {
        if (!props.mrzLoading && !props.mrzValid) {
            this.setState({
                waitUserAction: true,
                popupMessage: 'მონაცემები ვერ ამოვიკითხე, სცადე კიდევ ერთხელ',
                mustReInit: true
            })
        } else if (props.mrzInfo && this.props.mrzInfo !== props.mrzInfo) {
            this.setState({ waitUserAction: true, popupMessage: 'უკანა მხარე წარმატებით დასკანერდა' })
        }

        if (props.mrzLoading) {
            this.setState({ waitUserAction: true })
        }
    }

    render() {
        const canvasClassnames = classNames({
            'blur-effect': this.state.waitUserAction
        })

        return (
            <div className='ScanCard'>
                <p className='title'>
                    {this.state.scanDetails.info}
                </p>
                <p className='subtitle'>
                    მიიტანე ბარათი ვებ კამერასთან და დაუმიზნე ჩარჩოს
                </p>

                <Stepper currentStep={this.state.scanDetails.activeSide} />

                <div>
                    <video id='webcam' width='700' height='500' style={{ display: 'none' }} src=''></video>
                    {this.state.waitUserAction && <div className='proceed'>
                        {!this.props.mrzLoading && <div className='items'>
                            {!this.state.mustReInit && <svg xmlns='http://www.w3.org/2000/svg' width='45.906' height='46' viewBox='0 0 45.906 46'>
                                <path d='M703.958,361.11a1.942,1.942,0,0,0-1.938,1.944v1.957a19.092,19.092,0,0,
                                1-19.071,19.1h-0.011a19.114,19.114,0,0,1,.011-38.227h0.011a18.93,18.93,0,0,1,
                                7.751,1.655,1.942,1.942,0,0,0,1.578-3.55A22.77,22.77,0,0,0,682.962,342h-0.013a23,
                                23,0,0,0-.013,46h0.013A22.974,22.974,0,0,0,705.9,365.012v-1.958A1.942,1.942,0,0,0,
                                703.958,361.11Zm-2.528-11.623a1.936,1.936,0,0,0-2.742,
                                0l-15.739,16.988-4.932-4.943a1.941,
                                1.941,0,1,0-2.742,2.748l6.3,6.317a1.934,1.934,0,0,0,2.742,
                                0l17.11-18.362A1.945,1.945,0,0,
                                0,701.43,349.487Z' transform='translate(-660 -342)' />
                            </svg>
                            }

                            {this.state.mustReInit && <svg className='clear' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'>
                                <path d='M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z'></path>
                            </svg>}

                            <p>{this.state.popupMessage}</p>

                            <button onClick={() => this.nextAction()}>
                                {this.state.mustReInit ? 'თავიდან' : 'შემდეგი'}
                            </button>
                        </div>
                        }
                        {this.props.mrzLoading
                            && <p className='mrz-loading'>მონაცემები მუშავდება...</p>
                        }
                    </div>}
                    <div className='wrapper'>
                        <canvas id='canvas' className={canvasClassnames} width='700' height='500'></canvas>
                        <canvas id='canvas2' width='700' height='500' style={{ display: 'none' }}></canvas>
                        <canvas id='canvas3' width='700' height='500'></canvas>
                        <canvas id='frame' width='700' height='500'></canvas>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const {
        mrzInfo,
        mrzValid,
        mrzLoading,
        attemptNumber
    } = state.remoteIdCommon

    return {
        mrzInfo,
        mrzValid,
        mrzLoading,
        attemptNumber
    }
}

export default withRouter(connect(mapStateToProps, { readMRZ })(ScanCard))
