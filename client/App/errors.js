const handleErrors = (err) => {
    if (!err.response) {
        return
    }

    console.log(err)
}

export default handleErrors
