import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import { withRouter } from 'react-router'
import { Grid } from 'react-flexbox-grid'

import RemoteId from './RemoteId'

import './index.scss'

class App extends Component {
    // eslint-disable-next-line class-methods-use-this
    componentDidMount() {
        document.getElementById('loader').style.display = 'none'
    }

    // eslint-disable-next-line class-methods-use-this
    render() {
        return (
            <Grid className='App'>
                <div className='main-container'>
                    <Route path='/' component={RemoteId} />
                </div>
            </Grid>
        )
    }
}

export default withRouter(App)
