import React, { Component } from 'react'
import axios from 'axios'
import io from 'socket.io-client'

import {
    Card, Typography, Button, Popover, Chip, CardContent, TextField, MenuItem, Popper, CircularProgress
} from '@material-ui/core'

import {
    fieldNameToDbName,
    dbNameToFieldName
} from './FieldMapping'
import MyModal from './MyModal'

export default class Verification extends Component {
    constructor(props) {
        super(props)
        this.state = {
            groups: [
                [
                    [
                        'სახელი',
                        'First Name',
                        'Gender'
                    ],
                    [
                        'გვარი',
                        'Last Name',
                        'Personal No'
                    ],
                ],
                [
                    [
                        'Date of Birth',
                        'Card No'
                    ],
                    [
                        'Date of Expiry',
                        'დაბადების ადგილი'
                    ]
                ]
            ],
            declineReasons: [
                'Fake',
                'Image Blur',
                'Wrong Doc',
                'Face Doesn\'t Match',
                'Document Doesn\'t Match',
                'Other',
            ],
            selectOpen: false,
            selectAnchor: null,
            accepted: null,
            imageOpen: false,
            imageAnchor: null,
            zoommedImageLink: '',
            frontImageLoaded: false,
            backImageLoaded: false,
            selfieImageLoaded: false
        }
    }

    setColumns = (curItem) => {
        const res = curItem ? JSON.parse(curItem.result) : null
        console.log(res)
        this.setState({...res})
    }

    verify = (sessionId, startTime, newResult) => {
        axios({
            url: `/api/dashboard/verify`,
            data: {
                sessionId,
                startTime,
                newResult,
            },
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        })
        .then(() => { this.props.deleteFromList(sessionId) })
        .catch(err => console.log(err))
    }

    reject = (sessionId, startTime, reason) => {
        axios({
            url: `/api/dashboard/reject`,
            data: {
                sessionId,
                startTime,
                reason,
            },
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        })
        .then(() => { this.props.deleteFromList(sessionId) })
        .catch(err => console.log(err))
    }

    getLink = (side) => {
        const scanList = Object.keys(this.props.openedSessionObject).filter(k => k.startsWith(`${side}ScanSuccess`))
        const scanSuccessInd = scanList.filter(k => this.props.openedSessionObject[k])[0].split(`${side}ScanSuccess`)[1]
        return this.props.openedSessionObject[`${side}ImageLink${scanSuccessInd}`]
    }

    getFrontImageLink = () => {
        if (this.props.openedSessionObject) {
            return this.props.s3Images[this.getLink('front')]
        }
    }

    getBackImageLink = () => {
        if (this.props.openedSessionObject) {
            return this.props.s3Images[this.getLink('back')]
        }
    }

    renderImages = () => 
        <div className="img-cmp">
            <div className="card-img">
                <div>
                    <img 
                        className={ this.state.frontImageLoaded ? '' : 'hide' }
                        src={ this.getFrontImageLink() } 
                        onClick={ (e) => this.handleImageZoomIn(e) } 
                        onLoad={ (e) => this.handleImageLoad('front') } /> 
                    <div className="img-progress"><CircularProgress className={ this.state.frontImageLoaded ? 'hide' : '' } /></div> 
                </div>
            </div>

            { !this.props.passport && 
                <div className="card-img">
                    <div>
                        <img 
                            className={ this.state.backImageLoaded ? '' : 'hide' }
                            src={ this.getBackImageLink() } 
                            onClick={ (e) => this.handleImageZoomIn(e) } 
                            onLoad={ (e) => this.handleImageLoad('back') } /> 
                        <div className="img-progress"><CircularProgress className={ this.state.backImageLoaded ? 'hide' : '' } /></div>
                    </div>
                </div>
            }

            <div className="card-img">
                <div>
                    <img 
                        className={ this.state.selfieImageLoaded ? '' : 'hide' }
                        src={ this.props.s3Images[this.props.openedSessionObject.selfieImageLink] } 
                        onClick={ (e) => this.handleImageZoomIn(e) } 
                        onLoad={ (e) => this.handleImageLoad('selfie') } /> 
                    <div className="img-progress"><CircularProgress className={ this.state.selfieImageLoaded ? 'hide' : '' } /></div>
                </div>
            </div>
            <Popper open={ this.state.imageOpen } anchorEl={this.state.imageAnchor }>
                <div><img className="zoom" src={ this.state.zoommedImageLink } onClick={ (e) => this.handleImageZoomOut() } /></div>
            </Popper>
        </div>

    renderDeclineReasons = () => 
        this.state.declineReasons.map(reason => 
            <Button color="secondary" onClick={ (e) => this.handleDeclineSelect(e, this.handleClose) }>{ reason }</Button>
        )

    renderInput = (field, key, disabled) => {
        switch(key) {
            case 'gender':
                return <TextField
                            disabled={ disabled }
                            select
                            className="text-field gender-select"
                            InputLabelProps={{ shrink: true, style: {
                                fontSize: 19
                            }}} 
                            label={ field }
                            value={ this.state[key] } 
                            onChange={ (e) => this.handleChange(e, key) }
                        >
                            <MenuItem key="M" value="M">
                                M
                            </MenuItem>
                            <MenuItem key="F" value="F">
                                F
                            </MenuItem>
                        </TextField>

            case 'idNum':
                return <TextField 
                            disabled={ disabled }
                            className="text-field" 
                            InputLabelProps={{ shrink: true, style: {
                                fontSize: 19
                            }}} 
                            inputProps={{
                                maxLength: 11,
                                style: {
                                    fontSize: 19
                                }
                            }}
                            label={ field } 
                            value={ this.state[key] }
                            onChange={ (e) => this.handleChange(e, key, /^[0-9]+$/) } 
                        />
            case 'dob':
            case 'expDate':
                return <TextField 
                            disabled={ disabled }
                            className="text-field with-helper" 
                            InputLabelProps={{ shrink: true, style: {
                                fontSize: 19
                            }}} 
                            inputProps={{
                                style: {
                                    fontSize: 19
                                }
                            }}
                            label={ field } 
                            value={ this.state[key] }
                            onChange={ (e) => this.handleChange(e, key, /^[0-9.]+$/) } 
                            helperText="dd.mm.yyyy"
                        />

            default:
                return <TextField 
                            disabled={ disabled }
                            className="text-field" 
                            InputLabelProps={{ shrink: true, style: {
                                fontSize: 19
                            }}} 
                            inputProps={{
                                style: {
                                    fontSize: 19
                                }
                            }}
                            label={ field } 
                            value={ this.state[key] }
                            onChange={ (e) => this.handleChange(e, key) } 
                        />
        }
    }

    renderPropertyCol = (col) => 
        <div className="property-col">
            { col.map(field => 
                <div className="field-wrapper">
                    { this.state.accepted !== null ? 
                        <React.Fragment>{ this.renderInput(field, fieldNameToDbName(field), true) }</React.Fragment>
                    :
                        <React.Fragment>{ this.renderInput(field, fieldNameToDbName(field)) }</React.Fragment>
                    }
                </div>
            )}
        </div>

    renderPropertyGroup = (group) => 
        <div className="property-group">
            { group.map(col => 
                this.renderPropertyCol(col)
            )}
        </div>
    

    handleImageZoomIn = (e) => {
        this.setState({
            imageOpen: true,
            imageAnchor: e.target,
            zoommedImageLink: e.target.src
        })
    }

    handleImageZoomOut = () => {
        this.setState({
            imageOpen: false,
            imageAnchor: null,
        })
    }

    handleChange = (e, propertyName, regex) => {
        if (e.target.value.match(regex)) {
            this.setState({ [propertyName]: e.target.value })
        }
    }

    handleClose = () => {
        this.setState({
            selectAnchor: null,
            selectOpen: false,
        })
    }

    handleAccept = () => {      
        const {
            nameEn,
            lNameEn,
            idNum,
            gender,
            dob,
            docNumber,
            expDate,
            name,
            surname,
            birthPlace
        } = this.state

        const newResult = {
            nameEn,
            lNameEn,
            idNum,
            gender,
            dob,
            docNumber,
            expDate,
            name,
            surname,
            birthPlace
        }

        this.verify(this.props.openedSessionObject.sessionId, this.props.openedSessionObject.startTime, JSON.stringify(newResult))
        this.setState({ accepted: true })
    }

    handleImageLoad = (imgName) => {
        this.setState({ [`${imgName}ImageLoaded`]: true })
    }

    handleDecline = (e) => {
        this.setState({
            selectOpen: true,
            selectAnchor: e.target
        })
    }

    handleDeclineSelect = (e, handleClose) => {
        handleClose()
        this.reject(this.props.openedSessionObject.sessionId, this.props.openedSessionObject.startTime, e.target.innerText)
        this.setState({ accepted: false })
    }

    componentDidMount = () => {
        this.setColumns(this.props.openedSessionObject)

        // const socket = io('http://localhost:4200')

        // socket.on('client-connect', data => {
        //     console.log(encodeURI('2020/01/17/15:14:17'))
        //     console.log(data)
        //     this.props.addItem(data.sessionId, data.startTime)
        // })
    }

    componentDidUpdate = (oldProps, oldState) => {
        if (JSON.stringify(oldProps) === JSON.stringify(this.props)) {
            return
        }

        if (this.props.reset) {
            this.setColumns(this.props.openedSessionObject)
            this.setState({
                selectOpen: false,
                selectAnchor: null,
                imageOpen: false,
                imageAnchor: null,
                zoommedImageLink: '',
                frontImageLoaded: false,
                backImageLoaded: false,
                selfieImageLoaded: false,
                accepted: null
            })
            this.props.resett()
        }
    }

    render() {
        return (
            <div className="Verification">    
                <Card className="card">
                    <Typography className="timer" variant="h3" component="h2">
                        3:00
                    </Typography>
                    <CardContent className="card-content">
                        { this.renderImages() }
                        <div className="card-details">
                            <Typography component={'div'} className="property-details" color="textSecondary" gutterBottom>
                                { this.state.groups.map( group =>
                                    this.renderPropertyGroup(group)
                                ) }
                            </Typography>
                        </div>
                        
                        { this.state.accepted === null ? 
                            <div className="card-actions">
                                <Button className="btn" size="large" variant="contained" color="primary" onClick={ this.handleAccept }>Accept</Button>
                                <Button className="btn" size="large" variant="contained" color="secondary" onClick={ this.handleDecline }>Decline</Button>
                                <Popover                            
                                    open={this.state.selectOpen}
                                    anchorEl={this.state.selectAnchor}  
                                    onClose={this.handleClose}
                                >
                                    <div id="decline-popper-content">
                                        { this.renderDeclineReasons() }
                                    </div>
                                </Popover>
                            </div>
                        : this.state.accepted ?
                            <div className="card-actions">
                                <Button className="btn accept-alert" disabled color='inherit' variant="outlined">Accepted</Button>
                            </div>
                        : 
                            <div className="card-actions">
                                <Button className="btn decline-alert" disabled color='inherit' variant="outlined">Declined</Button>
                            </div>
                        }  
                    </CardContent>                          
                </Card>
            </div>
        )
    }
}
