/* eslint-disable max-len */
import React, { Component } from 'react'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import PropTypes from 'prop-types'

export default class MyModal extends Component {
    state = { open: false }

    render() {
        return (
            <div>
                <button type='button' onClick={() => (this.setState({ open: true }))}>{(this.props.imgname).substr(0, (this.props.imgname).indexOf('Link')) }</button>
                <Modal
                    open={this.state.open}
                    onClose={() => (this.setState({ open: false }))}
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                    style = {{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <Fade in={this.state.open}>
                        <img src={this.props.imgsrc} style={{
                            border: '2px solid #000', maxWidth: '75vw', maxHeight: '75vh'
                        }}/>
                    </Fade>
                </Modal>
            </div>
        )
    }
}

MyModal.propTypes = {
    imgname: PropTypes.string,
    imgsrc: PropTypes.string
}
