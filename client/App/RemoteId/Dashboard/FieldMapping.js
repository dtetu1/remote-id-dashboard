/* eslint quote-props: ["error", "always"] */
/* eslint-env es6 */

const fieldMap = {
    'დაბადების ადგილი' : 'birthPlace',
    'Gender' : 'gender',
    'First Name' : 'nameEn',
    'სახელი' : 'name',
    'Last Name' : 'lNameEn',
    'გვარი' : 'surname',
    'Card No' : 'docNumber',
    'Personal No' : 'idNum',
    'Date of Birth' : 'dob',
    'Date of Expiry' : 'expDate',
}

const flippedFieldMap = (() => {
    const res = {}
    Object.keys(fieldMap).forEach((k) => {
        res[fieldMap[k]] = k
    })
    return res
})()

const fieldNameToDbName = fieldName => fieldMap[fieldName]
const dbNameToFieldName = dbName => flippedFieldMap[dbName]

module.exports = {
    fieldNameToDbName,
    dbNameToFieldName
}
