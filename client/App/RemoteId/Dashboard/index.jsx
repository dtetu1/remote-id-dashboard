/* eslint-disable max-len */
import React, { Component } from 'react'
import axios from 'axios'
import {
    List, ListItem, ListItemText, AppBar, Toolbar, MenuIcon, Typography, Button, Paper, Divider, IconButton, SearchIcon, InputBase, CircularProgress
} from '@material-ui/core'
import MyModal from './MyModal'
import './index.scss'
import Verification from './Verification'
import moment from 'moment'
// import websocket from 'websocket'

class Dashboard extends Component {
    state = {
        data: null,
        s3Images: {},
        openedSessionObject: null,
        reset: true
    }

    deleteFromList = (sessionId) => {
        this.setState({ data: this.state.data.filter(el => el.sessionId !== sessionId) })
    }

    addItem = (sessionId, startTime) => {
        console.log(sessionId, startTime)
        console.log(this.getItem(sessionId, startTime))
    }

    getItem = (sessionId, startTime) => {
        axios({
            url: `/api/dashboard/item/${sessionId}/${startTime.replace(/\//g, '_')}`,
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        })
        .then((res) => {
            this.setState({ data: [res.data.Item, ...res.data] })
        })
        .catch(err => console.log(err))
    }

    getItems = () => {
        axios({
            url: '/api/dashboard/items',
            method: 'GET',
            headers: { 'Content-Type': 'application/json' }
        })
        .then((res) => {
            this.setState({ data: res.data.Items.filter(i => !('status' in i))})
            this.state.data.forEach((element) => {
                Object.keys(element).forEach((item) => {
                    if (item.includes('Link')) {
                        this.getImages(element[item])
                    }
                })
            })
        })
        .catch(err => console.log(err))
    }

    getImages = (passedUrl) => {
        axios({
            url: '/api/dashboard/images',
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                s3imagekey: passedUrl.substring(passedUrl.indexOf('.com/') + 5)
            }
        })
        .then((res) => {
            const { s3Images } = this.state
            s3Images[passedUrl] = res.data
            this.setState({ s3Images })
        })
        .catch(err => console.log(err))
    }

    handleSelectItem = (item) => {
        if (!this.state.openedSessionObject || item.sessionId !== this.state.openedSessionObject.sessionId) {
            this.setState({
                openedSessionObject: this.state.data.filter(el => el.sessionId === item.sessionId)[0],
                selectedSessionId: item.sessionId,
                reset: true
            })
        }
    }

    resett = () => {
        this.setState({ reset: false })
    }

    // fetches data from dashboard api and initializes state
    componentDidMount = () => {
        this.getItems()
    }

    // renders child components passing them data and functions
    render() {
        return (
            <div className="Dashboard">
                <Paper square>
                    <img className="logo" src={ require('./pulsar-logo.png') } />
                </Paper>
                <div className="dashboard-content">
                    <div className="list">
                        { this.state.data === null ? 
                            <div className="progress"><CircularProgress /></div>
                        : this.state.data.length === 0 ?
                            <Typography variant="body1" style={{textAlign: "center"}}>No items</Typography>
                        :
                            <List>
                                {this.state.data
                                .sort((a, b) => (a.startTime > b.startTime) ? -1 : 1)
                                .map(item => {
                                    let itemText = null
                                    let time = moment(item.startTime, 'YYYY/MM/DD/h:mm:ss').add(4, 'hours').format('YYYY/MM/DD/h:mm:ss')
                                    
                                    if (item.result) {
                                        const result = JSON.parse(item.result)
                                        itemText = `${result.name} ${result.surname}`
                                    } else {
                                        itemText = item.startTime
                                    }
                                    
                                    return (
                                        <React.Fragment>
                                            <ListItem style={{fontSize: 10}} 
                                                button 
                                                key={item.sessionId} 
                                                selected={item.sessionId === this.state.selectedSessionId} 
                                                onClick={ () => this.handleSelectItem(item) }>
                                                <ListItemText primary={ itemText } secondary={ time }/>
                                            </ListItem>
                                            <Divider variant="horizontal" />
                                        </React.Fragment>
                                    )
                                })}
                            </List>
                        }
                    </div>
                    { this.state.openedSessionObject ?
                        this.state.openedSessionObject.cardType === 'card-id' ? 
                            <Verification 
                                { ...this.state } 
                                deleteFromList={ this.deleteFromList.bind(this) }
                                addItem={ this.addItem.bind(this) }
                                resett={ this.resett.bind(this) }
                            /> 
                        : 
                            <Verification
                                passport 
                                { ...this.state } 
                                deleteFromList={ this.deleteFromList.bind(this) }
                                addItem={ this.addItem.bind(this) }
                                resett={ this.resett.bind(this) }
                            />
                        :
                            <div className="empty-page">
                                <Typography gutterBottom variant="h3" component="h3" color="inherit">Select item from list...</Typography>
                            </div>
                    }
                </div>
            </div>
        )
    }
}

export default Dashboard
