import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import { withRouter } from 'react-router'

import Dashboard from './Dashboard'

class RemoteId extends Component {
    // eslint-disable-next-line class-methods-use-this
    render() {
        return (
            <div className='RemoteId' >
                <Route path='/' component={Dashboard} />
            </div>
        )
    }
}

export default withRouter(RemoteId)
