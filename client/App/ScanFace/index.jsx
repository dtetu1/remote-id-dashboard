import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import classNames from 'classnames'
import {
    initTracker,
    pauseTracking,
    stopTracking
} from './BlinkJS'

import {
    checkFaces,
    clearState
} from './actions'

import Stepper from '../Common/Stepper'
import FaceImage from './table.png'

import './index.scss'

class ScanFace extends React.Component {
    constructor(props) {
        super(props)
        this.videoRef = React.createRef()
        this.canvasRef = React.createRef()

        this.state = {}

        this.onBlinkSuccess = this.onBlinkSuccess.bind(this)
    }

    componentDidMount() {
        const { state } = this.props.location
        if (state) {
            const { frontImage } = state

            this.setState({ frontImage })

            initTracker(
                this.videoRef.current,
                this.canvasRef.current,
                this.onBlinkSuccess
            )
        }
    }

    componentWillUnmount() {
        stopTracking()
        this.props.clearState()
    }

    onBlinkSuccess(selfie) {
        console.log('selfie', selfie)
        if (selfie) {
            const { frontImage } = this.state
            pauseTracking()
            this.props.checkFaces(frontImage, selfie)
        }
    }

    goTo(page) {
        if (page === 'successPage') {
            this.props.history.push('/success')
        } else if (page === 'homePage') {
            this.props.history.push('/')
        }
    }

    render() {
        const videoBoxClassnames = classNames({
            'blur-effect': this.props.loading || this.props.complete
        })

        return (
            <div className='ScanFace'>
                <p className='title'>
                    მიიტანე სახე კამერასთან, დაახამხამე
                </p>
                <p className='subtitle'>
                    მიიტანე სახე ვებ კამერასთან
                </p>

                <Stepper currentStep={'face'} />

                <div className='wrapper'>
                    <video
                        ref={this.videoRef}
                        width='700'
                        height='500'
                        className={videoBoxClassnames}
                        autoPlay={true}>
                    </video>

                    {!this.props.complete && <canvas
                        ref={this.canvasRef}
                        width='700'
                        height='500'>
                    </canvas>}

                    {this.props.loading
                        && <div className='addional-layer'>
                            <p className='loading'>მონაცემები მუშავდება...</p>
                        </div>}

                    <div className='face'>
                        <img src={FaceImage} alt='Face'/>
                    </div>

                    {this.props.complete
                        && <div className='addional-layer'>
                            {this.props.matchingSuccess && <div>
                                <svg xmlns='http://www.w3.org/2000/svg' width='45.906' height='46' viewBox='0 0 45.906 46'>
                                    <path d='M703.958,361.11a1.942,1.942,0,0,0-1.938,1.944v1.957a19.092,19.092,0,0,
                                    1-19.071,19.1h-0.011a19.114,19.114,0,0,1,.011-38.227h0.011a18.93,18.93,0,0,1,
                                    7.751,1.655,1.942,1.942,0,0,0,1.578-3.55A22.77,22.77,0,0,0,682.962,342h-0.013a23,
                                    23,0,0,0-.013,46h0.013A22.974,22.974,0,0,0,705.9,365.012v-1.958A1.942,1.942,0,0,0,
                                    703.958,361.11Zm-2.528-11.623a1.936,1.936,0,0,0-2.742,
                                    0l-15.739,16.988-4.932-4.943a1.941,
                                    1.941,0,1,0-2.742,2.748l6.3,6.317a1.934,1.934,0,0,0,2.742,
                                    0l17.11-18.362A1.945,1.945,0,0,
                                    0,701.43,349.487Z' transform='translate(-660 -342)' />
                                </svg>

                                <p>ვერიფიკაცია წარმატებით გაიარე</p>

                                <button onClick={() => this.goTo('successPage')}>
                                    შედეგების ნახვა
                                </button>
                            </div>}
                            {!this.props.matchingSuccess && <div>
                                <svg className='clear' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'>
                                    <path d='M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z'></path>
                                </svg>

                                <p>ვერიფიკაცია ვერ გაიარე</p>

                                <button onClick={() => this.goTo('homePage')}>
                                    თავიდან დაწყება
                                </button>
                            </div>}
                        </div>}
                </div>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const {
        loading,
        complete,
        matchingSuccess
    } = state.scanFace

    return {
        loading,
        complete,
        matchingSuccess
    }
}

export default withRouter(connect(mapStateToProps, {
    checkFaces,
    clearState
})(ScanFace))
