import {
    createStore as createReduxStore,
    combineReducers,
    applyMiddleware
} from 'redux'
import reduxThunk from 'redux-thunk'

import remoteIdCommonReducers from './App/reducers'

const reducers = combineReducers({
    remoteIdCommon: remoteIdCommonReducers,
})

const rootReducer = (state, action) => (
    reducers(state, action)
)

export default () => {
    const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createReduxStore)
    const store = createStoreWithMiddleware(rootReducer)

    return store
}
